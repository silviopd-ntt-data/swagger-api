package com.swagger.swagggerapi.Dto;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

@Data
public class ArticHistoricRequestDTO {

    //    @NotNull
//    @Size(min = 1, message = "user name should have at least 2 characters")
    private int articleTic;

    //    @NotNull
//    @Size(min = 1, message = "user name should have at least 2 characters")
    private int[] fieldTics;

    private long[] historicTics;
    private long[] parentHistoricTics;
    private int[] colorTics;
    private int[] sizeTics;
    private String creationDate;
    private long[] valueTics;
    private String value;
    private boolean isActualValue = true;
    private String creationUser;


    public long[] getHistoricTics() {
        if (historicTics == null) {
            historicTics = new long[0];
        }
        return historicTics;
    }

    public long[] getParentHistoricTics() {
        if (parentHistoricTics == null) {
            parentHistoricTics = new long[0];
        }
        return parentHistoricTics;
    }

    public int[] getColorTics() {
        if (colorTics == null) {
            colorTics = new int[0];
        }
        return colorTics;
    }

    public int[] getSizeTics() {
        if (sizeTics == null) {
            sizeTics = new int[0];
        }
        return sizeTics;
    }

    public long[] getValueTics() {
        if (valueTics == null) {
            valueTics = new long[0];
        }
        return valueTics;
    }

    public String getValue() {
        if (value == null) {
            value = "";
        }
        return value;
    }

    public String getCreationDate() {
        if (creationDate == null) {
            creationDate = "";
        }
        return creationDate;
    }

    public String getCreationUser() {
        if (creationUser == null) {
            creationUser = "";
        }
        return creationUser;
    }
}
